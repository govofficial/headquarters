const Discord = require('discord.js');
const chalk = require('chalk');
const sql = require('sqlite');
const fs = require('fs');
const black = require('./config/blacklist.json');
const image = require('./images/images.json');
const config = require('./config/settings.json');
const h = require('./config/help.json').help;
const quotes = require('./Fun/quotes.json');
const ball = require('./Fun/8ball.json').ball;
const creatists = require('./Fun/creations.json');
const ddif = require('return-deep-diff');
const client = new Discord.Client();
sql.open('./score.sqlite');

client.login(config.token);
function commandIs(str, msg){
    return msg.content.toLowerCase().startsWith("+" + str)
}
function pluck(array){
  return array.map(function(item) {return item["name"]; });
}
function hasRole(mem, roles) {
    if(pluck(mem.roles).includes(roles)){
        return true;
    } else{
        return false;
    }
}
function clean(text){
    if(typeof(text) === "string")
        return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
    else
        return text;
};

client.on('ready', () =>{
    console.log(chalk.bgGreen('Headquarters v2.0 is Online!!!'));
    client.user.setGame("+help | We Lit Boi!");
});
client.on("disconnect", () => {
  console.log(`The Bot has been disconnected: ${new Date()}`)
});
client.on("reconnecting", () => {
  console.log(`The Bot has been Reconnected: ${new Date()}`)
});

//Global Vars
var owner = config.ownerid;
//let msgpoints = JSON.parse(fs.readFileSync('./level/points.json', 'utf8'));
//.catch(error => console.log(error)) is Handy Dandy

/////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


client.on('message', message => {
    var args = message.content.split(/[ ]+/);
    if(message.channel.type === 'dm') return;
    if(message.author.bot) return;
    if(black.ids.includes(message.author.id)) return;


sql.get(`SELECT * FROM scores WHERE userId ='${message.author.id}'`).then(row => {
  if(!row) {
    sql.run('INSERT INTO scores (userId, points, level) VALUES (?, ?, ?)', [message.author.id, 1, 0]);
  } else {
    let curLevel = Math.floor(0.1 * Math.sqrt(row.points + 1));
    if (curLevel > row.level){
      row.level = curLevel;
      sql.run(`UPDATE scores SET points = ${row.points + 1}, level = ${row.level} WHERE userId = ${message.author.id}`);
      message.reply(`You've leveled up\n **Level:** ${curLevel}`);
    }
    sql.run(`UPDATE scores SET points = ${row.points + 1} WHERE userId = ${message.author.id}`);
  };
  }).catch(() => {
    console.error;
    sql.run('CREATE TABLE IF NOT EXISTS scores (userId TEXT, points INTEGER, level INTEGER)').then(() => {
      sql.run('INSTERT INTO scores (userId, points, level) VALUES (?, ?, ?)', [nessage.author.id, 1, 0]);
  });
});





    if(commandIs("hello", message)){
        message.channel.sendMessage('`print("hello world, ' + message.author.username + '")`')
    }
    if(commandIs("say", message)){
        if(message.member.hasPermission("MANAGE_MESSAGES") || (message.member.author === owner)){
        if(args.length === 1){
            message.channel.sendMessage("You did not define an Argument.\nUsage: `$say 'msg'`");
        } else {
            message.channel.sendMessage(args.join(" ").substring(5));
        }
        } else {
        message.channel.sendMessage("You do not have the Appropiate Permissions. `Manage_Messages`");
        }
    }
    if(message.content === 'ayy' || message.content === 'Ayy'){
        message.channel.sendMessage("Lmao")
    }
    if(commandIs("help", message)){
        message.author.send(h.join("\n"))
    }
    if(commandIs("ping", message)){
        message.channel.sendMessage(`Pong! | ${Math.floor(client.ping)}ms`)
    }
    if(commandIs("avatar", message)){
        if(message.mentions.users.size < 1){
            message.channel.sendMessage(message.author.avatarURL)
        } else {
            message.channel.sendMessage(message.mentions.users.first().avatarURL);
        }
    }
    if(commandIs("playing", message)){
        if (message.author.id !== owner) return;
        client.user.setGame(args.join(" ").substring(9))
    }
    if(commandIs("blacklist", message)){
        if(message.author.id !== owner) return;
        let add = args[1];
        if(add === "add"){
          let id = args[2];
          black.ids.push(id);
          client.channels.get('282618142000414720').sendMessage(id + "\nHas been blacklisted please add to Official list when able");
        } else {
        const embed = new Discord.RichEmbed()
        .setTitle("List of Blacklisted Peeps")
        .setDescription(black.names.join(',\n'))

        message.channel.sendEmbed(embed);
      }
    }
    if(commandIs("prune", message)){
        if(message.member.hasPermissions(["MANAGE_MESSAGES", "ADMINISTRATOR"]) || message.author.id === owner){
            let messagecount = parseInt(args[0]);
            message.channel.fetchMessages({limit: args[1]})
                .then(messages => message.channel.bulkDelete(messages));
            message.channel.sendMessage(args[1] + " messages has been removed from existence :thumbsup:")
        } else {
            message.channel.sendMessage("You do not have the Appropiate Perms. `Manage_Messages, Administrator`")
        }
    }
    if(commandIs("justdoit", message)){
        message.channel.sendMessage("http://i.imgur.com/VivBAZ2.png")
    }
    if(commandIs("guildavatar", message)){
        message.channel.sendMessage(message.guild.iconURL)
    }
    if(commandIs("awshit", message)){
        message.channel.sendFile(image.awshit[Math.floor(Math.random() * image.awshit.length)])
    }
    if(commandIs("woah", message)){
        message.channel.sendFile("http://3.bp.blogspot.com/-LOv03ZhdsRg/VspzL9OvX3I/AAAAAAAABAA/KXsdmNqfT8s/s1600/7c14fb2510a2da117b1d7c0b23c18177.gif")
    }
    if(commandIs("info", message)){
        if(message.mentions.users.size < 1){
        const embed = new Discord.RichEmbed()
        .setTitle(`Info on ${message.author.username}#${message.author.discriminator}`)
        .setColor('#0000ff')
        .setFooter(`Requested by ${message.author.username} at ${message.createdAt}`)
        .setThumbnail(message.author.displayAvatarURL)
        .addField("Nickname:", message.author.nickname, true)
        .addField("User ID:", message.author.id, true)
        .addField("Avatar:", message.author.avatarURL, true)
        .addField("Status:", message.author.presence.status, false);

        message.channel.sendEmbed(embed);
        } else {
        const embed = new Discord.RichEmbed()
        .setTitle(`Info on ${message.mentions.users.first().username}#${message.mentions.users.first().discriminator}`)
        .setColor('#0000ff')
        .setFooter(`Requested by ${message.mentions.users.first().username} at ${message.createdAt}`)
        .setThumbnail(message.mentions.users.first().displayAvatarURL)
        .addField("Nickname:", message.mentions.users.first().nickname, true)
        .addField("User ID:", message.mentions.users.first().id, true)
        .addField("Avatar:", message.mentions.users.first().avatarURL, true)
        .addField("Status:", message.mentions.users.first().presence.status, false);

        message.channel.sendEmbed(embed);
        }
    }
    if(commandIs('eval', message)){
        if(message.author.id !== owner) return;
        try {
            var code = args.join(" ").substring(6);
            var evaled = eval(code);

            if(typeof evaled !== "string")
                evaled = require("util").inspect(evaled);

            message.channel.sendCode("xl", clean(evaled));
        } catch (err) {
            message.channel.sendMessage(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
        }
    }
    if(commandIs('quotes', message)){
      let name = args[1];
      if(!name){
         return message.channel.sendMessage(`List of Quoter's\n${Object.keys(quotes).join(', \n')}`);
      } else if(Object.keys(quotes).includes(name)){
        let quote = quotes[name][Math.floor(Math.random() * quotes[name].length)];
        message.channel.sendMessage(quote)
      } else{
        message.channel.sendMessage(`Thats not a Quoter\n${Object.keys(quotes).join(', \n')}`)
      }
    }
    if(commandIs('creations', message)){
        let artist = args[1];
        if(!artist){
            message.channel.sendMessage(`List of Artists:\n${Object.keys(creatists).join(', \n')}`)
        } else if(Object.keys(creatists).includes(artist)){
          let creation = creatists[artist][Math.floor(Math.random() * creatists[artist].length)]
          message.channel.sendMessage(creation);
        } else{
          message.channel.sendMessage(`List of Artists:\n${Object.keys(creatists).join(', \n')}`)
        }
    }
    if(commandIs('rank', message)){
      sql.get(`SELECT * FROM scores WHERE userId = '${message.author.id}'`).then(row => {
        if (!row) return message.reply('You are Nothing to me');
        message.reply(`\nYou are level ${row.level}\nYou have ${row.points} Exp`);
      })
    }
    if(commandIs('kick', message)){
      let kickUser = message.mentions.users.first();
      if(!kickUser){
        message.channel.sendMessage("No user Specified, `Usages: +kick (Mention)`")
      } else if(message.member.hasPermissions(["KICK_MEMBERS"])){
        message.guild.member(kickUser).kick();
      } else {
        message.channel.sendMessage("Ya don't have the needed Perms `KICK_MEMBERS`");
      }
    }
    if(message.mentions.everyone === true){
      message.reply("Don't fucking do that you Afro Fucking Shit Eater");
    }
    if(message.tts === true){
      message.reply("SHUT THE FUCK UP IM TRYING TO CONCENTRATE");
    }
    // if(message.content.includes("lmao")){
    //   function response(message){
    //     return message.react('😑');
    //   }
    //
    //   response(message);
    // }
    // if(message.content.includes("lol")){
    //   function response(message){
    //     return message.react('😑')
    //   }
    //
    //   response(message);
    // }
    if(commandIs('tableflip', message)){
      if(message.mentions.users.size < 1){
        message.channel.sendMessage("Mention someone to use this")
      } else{
        message.channel.sendMessage(`${message.author.username} Threw a Table at ${message.mentions.users.first().username}\n(╯°□°）╯︵ ┻━┻`)
      }
    }
    if(commandIs('dice', message)){
      let dice = args[1];
      if(!dice){
        message.channel.sendMessage(`:8ball: | You rolled a ${Math.floor(Math.random() * 100)}`)
      } else{
        message.channel.sendMessage(`:8ball: | You rolled a ${Math.floor(Math.random() * dice)}`)
      }
    }
    if(commandIs('f', message)){
      let respect = args[1];
      if(!respect){
        message.channel.sendMessage(`**${message.author.username}** paid their respects to **Headquarters v3.0**`)
      } else {
        message.channel.sendMessage(`**${message.author.username}** paid their respects to **${args.join(' ').substring(3)}**`)
      }
    }
    if(commandIs('8ball', message)){
      let question = args[1];
      if(!question){
        message.channel.sendMessage("Ask a Question.")
      } else {
        message.channel.sendMessage(`:8ball: | ${ball[Math.floor(Math.random() * ball.length)]}, ${message.author.username}`)
      }
    }
    if(commandIs('cookie', message)){
      if(message.mentions.users.size < 1){
        message.channel.sendMessage('Give someone a Cookie? or keep it?')
      } else {
        message.channel.sendMessage(`:cookie: | **${message.author.username}** has Given <@${message.mentions.users.first().id}> a **Cookie**`)
      }
    }
    if(commandIs("stats", message)){
      var date = new Date(client.uptime);
      var days = date.getUTCDate() - 1;
      var hours = date.getUTCHours();
      var minutes = date.getUTCMinutes();
      // let uptime = moment(new Date(new Date() - client.uptime)).format("D [days], H [hrs], m [mins], s [secs]");
      const embed = new Discord.RichEmbed()
      .setAuthor(message.author.username, message.author.displayAvatarURL, "https://discord.gg/SJd3DrT")
      .setDescription(message.author.presence.game ? message.author.presence.game.name : "Not Playing Anything")
      .addField("Guilds", client.guilds.size, false)
      .addField("Channels", client.channels.size, false)
      .addField("Emojis", client.emojis.size, false)
      .addField("Users", client.users.size, false)
      .addField("Uptime", `${days} days, ${hours} hrs, ${minutes} mins`, false);

      message.channel.sendEmbed(embed);
    }
    if(commandIs("createrole", message)){
      let guild = message.guild;
      let roleName = args[1];
      let mentionHoist = args[2];
      let roleColor = args[3];
      if(!message.member.hasPermissions(["MANAGE_ROLES_OR_PERMISSIONS"])){
        message.channel.sendMessage("Ya don't have the needed Perms `MANAGE_ROLES_OR_PERMISSIONS`");
      }else if(!roleName || !roleColor || !mentionHoist){
        message.channel.sendMessage("Incorrect usage | `+createrole [rolename] [yes/no] [Hex Color Code]`");
      }else if(mentionHoist === 'no' || mentionHoist === 'No'){
        guild.createRole({name: roleName, color: roleColor, hoist: false, mentionable: false}).catch(error => message.channel.sendMessage('Incorrect usage | `+createrole [rolename] [yes/no] [Hex Color Code]`'))
      }else{
        guild.createRole({name: roleName, color: roleColor, hoist: true, mentionable: true}).catch(error => message.channel.sendMessage('Incorrect usage | `+createrole [rolename] [yes/no] [Hex Color Code]`'))
      }

    }
    if(commandIs("giverole", message)){
      let giveRole = message.mentions.users.first();
      if(!giveRole){
        return message.channel.sendMessage('Incorrect Usage | `+giverole @mention roleid`');
      }
      let role = args.join(' ').substring(args[1].length + args[0].length + 2);
      if(!message.member.hasPermissions(["MANAGE_ROLES_OR_PERMISSIONS"])){
        message.channel.sendMessage("You don't have the required Perms `MANAGE_ROLES_OR_PERMISSIONS`")
      } else if(!message.guild.roles.exists('name', role)){
        message.channel.sendMessage(`${role} Doesn't Exist`);
      }else {
        let roleId = message.guild.roles.find('name', role).id;
        message.guild.member(giveRole).addRole(roleId).catch(error => message.channel.sendMessage('Incorrect Usage | `+giverole @mention roleid`'))
      }
    }
    if(commandIs("takerole", message)){
      let role = args.join(' ').substring(args[0].length + 1);
      if(!message.guild.roles.exists('name', role)){
        message.channel.sendMessage(`${role} Doesn't Exist`)
      } else if(!role){
        message.channel.sendMessage(`Incorrect Usage | \`+takerole role\``)
      } else{
        let roleId = message.guild.roles.find('name', role).id;
        if(!message.member.roles.has(roleId)){
          message.channel.sendMessage(`You do not have this role! ${role}`);
        } else{
          message.member.removeRole(roleId).catch(error => message.channel.sendMessage('Incorrect Usage | `+takerole role`'))
        }
      }
    }



})



client.on("guildMemberAdd", member =>{
  console.log(`A New Member has Joined The Golden Factions: ${member.user.username}\n${new Date()}`)
});
client.on("guildMemberRemove", member =>{
  console.log(`Someone has left The Golden Faction: ${member.user.username}\n${new Date()}`)
});
client.on("channelCreate", channel =>{
  console.log(`A New Channel has been Created: ${channel.name}\n${new Date()}`)
});
client.on("guildBanAdd", (guild, user) =>{
  console.log(`Someone just got banned Rip ${user.username}`)
  guild.channels.get('260928678525927426').sendMessage(`${user.username} has been Banned from the Faction :/\n${new Date()}`)
});
client.on("guildBanRemove", (guild, user) =>{
  console.log(`Someone just got unbanned gg ${user.username}`)
  guild.channels.get('260928678525927426').sendMessage(`${user.username} has been unBanned from the Faction :/\n${new Date()}`)
});

client.on("roleCreate", role =>{
  let guild = role.guild;
  if(guild.id === '259303959536205825') return;
  client.channels.get('260928678525927426').sendMessage(`A **New Role** has been **Created**!`);
});
client.on("roleDelete", role =>{
  let guild = role.guild;
  if(guild.id === '259303959536205825') return;
  client.channels.get('260928678525927426').sendMessage(`A Role called **${role.name}** has been **Deleted**!`);
});
client.on("roleUpdate", (oRole, nRole) =>{
  let guild = oRole.guild;
  if(guild.id === '259303959536205825') return;
  console.log(`\n${oRole.name} has been updated`);
  console.log(ddif(oRole, nRole));
});
